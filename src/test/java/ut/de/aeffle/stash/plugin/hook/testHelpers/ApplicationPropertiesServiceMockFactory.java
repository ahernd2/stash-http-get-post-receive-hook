/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.testHelpers;

import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ApplicationPropertiesServiceMockFactory {

	private ApplicationPropertiesService applicationPropertiesService;
	private URI baseUrl;

	public ApplicationPropertiesServiceMockFactory() {
		clear();
	}

	public ApplicationPropertiesServiceMockFactory clear() {
		createNewMocks();
		loadDefaults();
		return this;
	}

	private void createNewMocks() {
		applicationPropertiesService = mock(ApplicationPropertiesService.class);

		when(applicationPropertiesService.getBaseUrl()).thenReturn(baseUrl);
	}

	private void loadDefaults() {
        try {
            baseUrl = new URI("http://example.com/stash");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

	public ApplicationPropertiesService getContext() {
		return applicationPropertiesService;
	}
	
}
