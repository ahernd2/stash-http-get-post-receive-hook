/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.validators;


import com.atlassian.stash.setting.Settings;

public class LocationCountValidator extends Validator {
    public LocationCountValidator(Settings settings) {
        super(settings);
    }

    @Override
    public boolean isValid() {
        int locationCount = getLocationCount();

        if (locationCountIsSmallerThanOne(locationCount)
                || locationCountIsLargerThanLimit(locationCount, 10)) {
            addError("locationCount", "Location has to be in the range from 1 to 10.");

            return false;
        }
        return true;
    }

    private boolean locationCountIsSmallerThanOne(int locationCount) {
        if (locationCount < 1) {
            return true;
        }
        return false;
    }

    private boolean locationCountIsLargerThanLimit(int locationCount, int limit) {
        if (locationCount > limit) {
            return true;
        }
        return false;
    }

}
