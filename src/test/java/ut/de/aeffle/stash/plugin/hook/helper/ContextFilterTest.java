/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.user.StashAuthenticationContext;
import de.aeffle.stash.plugin.hook.helper.ContextFilter;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import org.junit.Before;
import org.junit.Test;
import ut.de.aeffle.stash.plugin.hook.testHelpers.RefChangeMockFactory;
import ut.de.aeffle.stash.plugin.hook.testHelpers.StashAuthenticationContextMockFactory;

import static org.junit.Assert.assertEquals;


public class ContextFilterTest {

	private StashAuthenticationContextMockFactory authenticationContextFactory = new StashAuthenticationContextMockFactory();
	private RefChangeMockFactory refChangeMockFactory = new RefChangeMockFactory();

    @Before
    public void beforeTestClearSettings() {
    	authenticationContextFactory.clear();
    	refChangeMockFactory.clear();
	}

    @Test
    public void testEmptyFiltersShouldReturnMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter("");
        httpLocationTranslated.setTagFilter("");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(true, result);
    }

    @Test
    public void testMatchingBranchFilterQueryShouldReturnMatch() {
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter(".*");
        httpLocationTranslated.setTagFilter("");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(true, result);
    }

    @Test
    public void testMatchingRegExShouldMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter("*");
        httpLocationTranslated.setTagFilter("");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(true, result);
    }

    @Test
    public void testMatchingTagsShouldMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/tags/special-tag");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter("");
        httpLocationTranslated.setTagFilter("^special.*");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(true, result);
    }

    @Test
    public void testNotMatchingBranchFilterShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter("test");
        httpLocationTranslated.setTagFilter("");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(false, result);
    }

    @Test
    public void testNotMatchingTagsShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/tags/special-tag");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter("");
        httpLocationTranslated.setTagFilter("^special$");
        httpLocationTranslated.setUserFilter("");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(false, result);
    }

    @Test
    public void testNotMatchingUserFilterShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();
        httpLocationTranslated.setBranchFilter(".*");
        httpLocationTranslated.setTagFilter("");
        httpLocationTranslated.setUserFilter("jane.doe");

        boolean result = contextFilter.checkIfFilterMatches(httpLocationTranslated);

        assertEquals(false, result);
    }
}
